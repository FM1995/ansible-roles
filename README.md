# Ansible Roles

#### Project Outline

Lets say me and the team have adopted in our organization and using it to automate different tasks, as the adoption grows the complexity of the playbooks may go out of control and therefore hard to maintain. Here come roles, where we can break up large playbooks in smaller manageable file and they are easily re-usable and can shared with other team members.
Another advantage of roles is that we can use existing roles from Ansible galaxy/Git repo’s to create common tasks like sql, nosql, nginx etc

#### Lets get started

Let’s take this for example, let’s say we wanted to create the same thing on a new sever like a digital ocean droplet

![Image 1](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image1.png)

Lets say we wanted to configure on a new server either a new EC2 instance or Digital ocean droplet, so what can be done for another server which is a different linux operating system is extracting the common tasks which can be re-used

Similar to a function where we can extract the common logic and use the function in different places with different parameters.

So the first thing we need is the folder for roles and we can start with create linux user and start_containers

![Image 2](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image2.png)

In those directories, will also have to create a mandatory folder called tasks, can then have the file for the playbook, in our case have called it main.yaml

![Image 3](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image3.png)

Can start off by creating the importing the tasks in main.yaml

![Image 4](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image4.png)

Can then do the same thing for the tasks docker-compose

![Image 5](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image5.png)

Now we can reference those roles in the playbook, and instead of tasks we can specify roles and reference it for creating linux user or starting docker containers.

![Image 6](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image6.png)

Lets test it out. Lets configure the infrastructure using terraform

```
terraform apply
```

![Image 7](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image7.png)

Can then run the playbook to connect to configure our instance

```
ansible-playbook deploy-docker-with-roles.yaml -I inventory_aws_ec2.yaml
```

![Image 8](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image8.png)

Further logs and see it is successful

![Image 9](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image9.png)

Lets also do the same for static files, in our case we will use our docker-compose file and create a directory called files to host it. 

The shows the docker compose file that we have directly in the role

![Image 10](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image10.png)

Now that we have the static file, we can now reference it now playbook without giving the full file path

![Image 11](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image11.png)

Running the playbook again, can see it ran successfully

```
ansible-playbook deploy-docker-with-roles.yaml -I inventory_aws_ec2.yaml
```

![Image 12](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image12.png)

We can also customize the roles with variables. We can create the file 

![Image 13](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image13.png)

Can then make the following changes to the docker compose tasks for the docker credentials

![Image 14](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image14.png)

Can then reference it in the variables file, like the below where the 

![Image 15](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image15.png)

Can also do include defaults which can overwrite the vars file in the folder defaults

![Image 16](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image16.png)

Can also do the same for create user with groups

![Image 17](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image17.png)

Can then execute the playbook again

![Image 18](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image18.png)

And see it is successfull

![Image 19](https://gitlab.com/FM1995/ansible-roles/-/raw/main/Images/Image19.png)


